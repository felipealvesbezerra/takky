/* jshint ignore:start */
angular.module('cache.template', []).run(['$templateCache', function ($templateCache) {
  'use strict';

  $templateCache.put('cadastro_empresa.html',
    "<ion-view view-title=\"Preferencias\">\n" +
    "  <ion-content>\n" +
    "     <div class=\"list\">\n" +
    "      \n" +
    "      <select class=\"form-control\">\n" +
    "          <option value=\"\">Selecione tipo de cadastro</option>\n" +
    "          <option value=\"0\">Física</option>\n" +
    "          <option value=\"1\">Jurídica</option>\n" +
    "    </select>\n" +
    "\n" +
    "      <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Nome exibição\">\n" +
    "      </label>\n" +
    "\n" +
    "       <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Razão social\" >\n" +
    "      </label>\n" +
    "\n" +
    "       <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Nome fantasia ou nome\">\n" +
    "      </label>\n" +
    "\n" +
    "      <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"CNPJ\">\n" +
    "      </label>\n" +
    "\n" +
    "       <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"CPF\">\n" +
    "      </label>\n" +
    "\n" +
    "        <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"E-mail\">\n" +
    "      </label>\n" +
    "\n" +
    "         <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Telefone 1\">\n" +
    "      </label>\n" +
    "\n" +
    "         <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Telefone 2\">\n" +
    "      </label>\n" +
    "\n" +
    "        <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Celular 1\">\n" +
    "      </label>\n" +
    "\n" +
    "        <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Celular 2\">\n" +
    "      </label>\n" +
    "\n" +
    "      <label class=\"item item-input\">\n" +
    "        <textarea rows=\"4\" placeholder=\"Detalhes\"></textarea>\n" +
    "      </label>\n" +
    "\n" +
    "      <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Site\">\n" +
    "      </label>\n" +
    "          \n" +
    "      <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"CEP\">\n" +
    "      </label>\n" +
    "       \n" +
    "        <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Endereço\">\n" +
    "      </label>\n" +
    "\n" +
    "       <label class=\"item item-input\">\n" +
    "        <input type=\"number\" placeholder=\"Número\">\n" +
    "      </label>\n" +
    "\n" +
    "        <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Bairro\">\n" +
    "      </label>\n" +
    "\n" +
    "        <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"Cidade\">\n" +
    "      </label>\n" +
    "\n" +
    "        <label class=\"item item-input\">\n" +
    "        <input type=\"text\" placeholder=\"UF\">\n" +
    "      </label>\n" +
    "\n" +
    "         <select class=\"form-control\">\n" +
    "            <option value=\"\">Selecione tipo de estacionamento</option>\n" +
    "            <option value=\"0\">Gratuito</option>\n" +
    "            <option value=\"1\">Pago</option>\n" +
    "            <option value=\"2\">Não poussi</option>\n" +
    "        </select>\n" +
    "\n" +
    "        <label class=\"item item-input\">\n" +
    "            <input type=\"text\" placeholder=\"Valor\">\n" +
    "        </label>  \n" +
    "\n" +
    "         <label class=\"item item-input\">\n" +
    "            <input type=\"text\" placeholder=\"Facebook\">\n" +
    "        </label>\n" +
    "\n" +
    "          <label class=\"item item-input\">\n" +
    "            <input type=\"text\" placeholder=\"Twitter\">\n" +
    "        </label>\n" +
    "\n" +
    "         <label class=\"item item-input\">\n" +
    "            <input type=\"text\" placeholder=\"Instagram\">\n" +
    "        </label>\n" +
    "        \n" +
    "\n" +
    "         <li class=\"item item-toggle\">\n" +
    "           Wifi gratuito\n" +
    "           <label class=\"toggle toggle-balanced\">\n" +
    "             <input type=\"checkbox\">\n" +
    "             <div class=\"track\">\n" +
    "               <div class=\"handle\"></div>\n" +
    "             </div>\n" +
    "           </label>\n" +
    "        </li>\n" +
    "        \n" +
    "\n" +
    "           <label class=\"item item-input\">\n" +
    "            <input type=\"file\" placeholder=\"Logo\">\n" +
    "        </label>\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "  </ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('mapa.html',
    "<ion-view view-title=\"Mapa\">\n" +
    "{{result}}\n" +
    " <ion-content>\n" +
    "      <div id=\"map\" data-tap-disabled=\"true\"></div>\n" +
    "<!--     <ion-footer-bar class=\"bar-dark\">\n" +
    "    {{result}}\n" +
    "      <a ng-click=\"centerOnMe()\" class=\"button button-icon icon ion-navigate\">Find Me</a>\n" +
    "    <button ng-click=\"login()\" class=\"button button-block button-balanced\">Oauth Sign In</button>\n" +
    "            \n" +
    "    </ion-footer-bar>\n" +
    " -->\n" +
    "\n" +
    "\n" +
    "  </ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('pesquisar.html',
    "<ion-view view-title=\"Pesquisar\">\n" +
    "  <ion-content class=\"padding\">\n" +
    "    <ion-list>\n" +
    "\n" +
    "            <div class=\"list card\">\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "          <i class=\"energized icon ion-fork\"></i>\n" +
    "              Comis & Bebis\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-ios-telephone\"></i>\n" +
    "              Assistência Técnica\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-ios-book\"></i>\n" +
    "            Educação / Aulas e Cursos\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-model-s\"></i>\n" +
    "              Carros e Serviços Automotivos\n" +
    "            </a>\n" +
    "\n" +
    "              <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-ios-home\"></i>\n" +
    "            Serviços Domésticos\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-person\"></i>\n" +
    "             Moda e Beleza\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-settings\"></i>\n" +
    "            Casa, Construção e Serviços Residenciais\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-medkit\"></i>\n" +
    "              Saúde\n" +
    "            </a>\n" +
    "\n" +
    "                <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-ios-paw\"></i>\n" +
    "            Animais de Estimação\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-plane\"></i>\n" +
    "            Viagens e Turismo\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-ios-people\"></i>\n" +
    "            Outros Profissionais\n" +
    "            </a>\n" +
    "\n" +
    "            <a href=\"#\" class=\"item item-icon-left\">\n" +
    "              <i class=\"energized icon ion-more\"></i>\n" +
    "             Outros Serviços\n" +
    "            </a>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "    </ion-list>\n" +
    "  </ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('preferencias.html',
    "<ion-view view-title=\"Preferencias\"  >\n" +
    " <ion-content class=\"padding\">\n" +
    "    <ion-list>\n" +
    "\n" +
    "\n" +
    "<!--       <div id=\"fb-root\" > </div>  -->\n" +
    "\n" +
    "        <a href=\"#\" class=\"item item-icon-left\" ng-click=\"testeface()\">\n" +
    "      <i class=\"dark icon ion-locked\"></i>\n" +
    "          Cadastrar / Entrar\n" +
    "        </a>\n" +
    "\n" +
    "          <a href=\"#\" class=\"item item-icon-left\" ng-click=\"testeface2()\">\n" +
    "      <i class=\"dark icon ion-person\"></i>\n" +
    "          Perfil\n" +
    "        </a>\n" +
    "\n" +
    "         <a href=\"#/tab/preferencias/cadastro\" class=\"item item-icon-left\">\n" +
    "          <i class=\"balanced icon ion-social-usd\"></i>\n" +
    "        Sou um profissional\n" +
    "        </a>\n" +
    "\n" +
    "        <a href=\"#\" class=\"item item-icon-left\">\n" +
    "          <i class=\"assertive icon ion-android-favorite\"></i>\n" +
    "          Favoritos\n" +
    "        </a>\n" +
    "\n" +
    "        <a href=\"#\" class=\"item item-icon-left\">\n" +
    "          <i class=\"dark icon ion-gear-b\"></i>\n" +
    "        Configuração\n" +
    "        </a>\n" +
    "\n" +
    "\n" +
    "        {{teste}}\n" +
    "         {{teste2}}\n" +
    "          {{teste3}}\n" +
    "           {{teste4}}\n" +
    "            {{teste5}}\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "    </ion-list>\n" +
    "  </ion-content>\n" +
    "</ion-view>\n"
  );


  $templateCache.put('shared/tabs.html',
    "<ion-tabs class=\"tabs-icon-top tabs-background-energized tabs-color-dark\">\n" +
    "\n" +
    "  <!-- Dashboard Tab -->\n" +
    "  <ion-tab title=\"Mapa\" icon-off=\"ion-map\" icon-on=\"ion-map\" href=\"#/tab/dash\">\n" +
    "\n" +
    "    <ion-nav-view name=\"tab-dash\"></ion-nav-view>\n" +
    "  </ion-tab>\n" +
    "\n" +
    "  <!-- Chats Tab -->\n" +
    "  <ion-tab title=\"Pesquisar\" icon-off=\"ion-ios-search-strong\" icon-on=\"ion-ios-search-strong\" href=\"#/tab/pesquisar\">\n" +
    "    <ion-nav-view name=\"tab-pesquisar\"></ion-nav-view>\n" +
    "  </ion-tab>\n" +
    "\n" +
    "  <!-- Account Tab -->\n" +
    "  <ion-tab title=\"Preferencias\" icon-off=\"ion-navicon-round\" icon-on=\"ion-navicon-round\" href=\"#/tab/preferencias\">\n" +
    "    <ion-nav-view name=\"tab-preferencias\"></ion-nav-view>\n" +
    "  </ion-tab>\n" +
    "\n" +
    "\n" +
    "</ion-tabs>\n"
  );
}])
/* jshint ignore:end */