angular.module('app.config', [])

.run(function($rootScope, $compile) {

// Criando Marcador no MAPS
criarMarker = function(){
  servicos.forEach(function (item){

      var marker = new google.maps.Marker({
      position: item.position,
      map: $rootScope.map,
      animation: google.maps.Animation.DROP,
      title: item.title,
      icon: item.icon,
      });


      var contentString = 
      '<div style="width: 200px;" class="" >'+
        '<div class="text-center">'+
          '<p style="border-bottom: 1px solid #E3E3E3;"><strong><i class="ion-ios-home"></i> ' + item.title + '</strong></p>'+
          '<p><a href="'+"google.com.br"+'"><img  style="width:100%;" src="'+ item.foto +'"></a></p>'+
        '</div>'+
        '<div class="">'+
          '<p style="padding-bottom: 3px;border-bottom: 1px solid #E3E3E3;">'+
          '<strong><i class="ion-ios-email"></i> Endereço: </strong>'+
          "pais"+"estado"+"cidade"+"bairro"+"endereco"+"numero"+"cep"
          +'<br>' +
          '<strong><i class="ion-calendar"></i> Dias de Culto: </strong>'+
          "igreja.dias_culto" + '</p>'+
        '</div>'+
        '<div class="text-center">'+
          '<a class="button button-small icon-left ion-ios-search-strong button-calm" href="'+"google.com.br"+'" ><strong>Detalhes</strong></a>'+
        '</div>'+
      '</div>';

    var compiled = $compile(contentString)($rootScope);

      
      google.maps.event.addListener(marker, 'click', function() {
        $rootScope.infowindow.setContent(contentString);
        $rootScope.infowindow.open($rootScope.map, this);
        // console.log(item.position)
        // $rootScope.map.setCenter(item.position);

      });
    });


  }


});
	



// Variasveis fora do $rootScope (Colocar elas com urgencia em .run $rootScope)
	var icon = {
        muitoBom: 'http://icon-icons.com/icons2/317/PNG/128/map-marker-icon_34392.png'
        , bom: 'http://www.sitmed.com.br/images/assistencia_icon.png'
        , medio: 'http://i.imgur.com/eNAvIvr.png'
        , ruim: 'http://i.imgur.com/uCRXqdV.png'
        , auto: 'content/img/icon/auto.png'
    }



	var servicos = [{

		title : "Frank Auto peças"
		,icon : "content/img/icon/model-s16.png"
		,foto : "http://blog.sigmacar.com.br/wp-content/uploads/2014/10/Oficina-Mecanica-em-SP-1.jpg"
		,position : {lat: -23.7156236, lng: -46.5381541 }

	},

	{

		title : "FDG Mecânica"
		,icon : "content/img/icon/model-s16.png"
		,foto : "http://www.becocomsaida.blog.br/wp-content/uploads/2008/10/oficina-mecanica1.jpg"
		,position : {lat: -23.7170482, lng: -46.5386821}

	}]

