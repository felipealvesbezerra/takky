
angular.module('app')

.run(function($ionicPlatform) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})


.config(function($stateProvider, $urlRouterProvider) {

  // Rota Padrão
  $urlRouterProvider.otherwise('/tab/pesquisar');

  // ROTAS 
  $stateProvider

    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'view/shared/tabs.html'
  })

    .state('tab.dash', {
    url: '/dash',
    views: {
        'tab-dash': {
          templateUrl: 'view/mapa.html',
          controller: 'mapaController'
        }
      }
  })

       .state('tab.pesquisar', {
    url: '/pesquisar',
    views: {
        'tab-pesquisar': {
          templateUrl: 'view/pesquisar.html',
        }
      }
  })

    .state('tab.preferencias', {
    url: '/preferencias',
    views: {
        'tab-preferencias': {
          templateUrl: 'view/preferencias.html',
           controller: 'loginCtrl'
        }
      }
  })

    .state('tab.cadastro', {
    url: '/preferencias/cadastro',
    views: {
        'tab-preferencias': {
          templateUrl: 'view/cadastro_empresa.html',
        }
      }
  })


});
