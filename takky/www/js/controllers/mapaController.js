angular.module("app")

.controller('mapaController', function($scope, $ionicLoading, $compile, $cordovaGeolocation, $rootScope, $compile,  $cordovaOauth) {


  $scope.loading = $ionicLoading.show({
          content: 'carregando',
          showBackdrop: false
  });



   function construir() {

    var posOptions = {timeout: 10000, enableHighAccuracy: false};

    $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function (position) {
        var latitude  = position.coords.latitude
        var longitude = position.coords.longitude

        var positionL = {
            lat: latitude
          , lng: longitude
        }
   
        var myLatlng = new google.maps.LatLng(latitude, longitude);
        
        var mapOptions = {
          center: myLatlng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);


        var infowindow = new google.maps.InfoWindow();
        $rootScope.infowindow = infowindow;
        
       

      
        var marker = new google.maps.Marker({
        position: positionL,
        map: map,
        animation: google.maps.Animation.DROP,
        title: 'Felipe teste ',
        // icon: imagens.muitoBom,
        });

        var contentString = 
        '<div style="width: 200px;" class="" id="infowindow_content">'+
          '<div class="text-center">'+
            '<p style="border-bottom: 1px solid #E3E3E3;"><strong><i class="ion-ios-home"></i> ' + "Felipe" + '</strong></p>'+
            '<p><a href="'+"google.com.br"+'"><img ng-cache style="width:100%;" ng-src="'+ "Felipe "+'"></a></p>'+
          '</div>'+
          '<div class="">'+
            '<p style="padding-bottom: 3px;border-bottom: 1px solid #E3E3E3;">'+
            '<strong><i class="ion-ios-email"></i> Endereço: </strong>'+
            "pais"+"estado"+"cidade"+"bairro"+"endereco"+"numero"+"cep"
            +'<br>' +
            '<strong><i class="ion-calendar"></i> Dias de Culto: </strong>'+
            "igreja.dias_culto" + '</p>'+
          '</div>'+
          '<div class="text-center">'+
            '<a class="button button-small icon-left ion-ios-search-strong button-calm" href="'+"google.com.br"+'" ><strong>Detalhes</strong></a>'+
          '</div>'+
        '</div>';

        // var compiled = $compile(contentString)($rootScope);

      google.maps.event.addListener(marker, 'click', function() {
        $rootScope.infowindow.setContent(contentString);
        $rootScope.infowindow.open(map, this);
        // $rootScope.map.setCenter(position);
        // console.log('oi')
      });



        $rootScope.map = map;
        criarMarker();

        $rootScope.map.setCenter(new google.maps.LatLng(latitude, longitude));
         $scope.loading = $ionicLoading.hide();


         }, function(err) {
           // error
           console.log(err)
        });

      }

      // google.maps.event.addDomListener(window, 'load', initialize);
      
      $scope.centerOnMe = function() {
        if(!$rootScope.map) {
          return;
        }

        $scope.loading = $ionicLoading.show({
          content: 'Getting current location...',
          showBackdrop: false
        });

        navigator.geolocation.getCurrentPosition(function(pos) {
          $rootScope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
          $scope.loading =  $ionicLoading.hide();
        }, function(error) {
          alert('Unable to get location: ' + error.message);
        });
      };
      
      $scope.clickTest = function() {
        alert('Example of infowindow with ng-click')
      };


      construir();

  });

