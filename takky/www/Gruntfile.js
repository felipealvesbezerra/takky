module.exports = function(grunt) {
    "use strict";
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);
    // Iniciando a configuração das tarefas
    grunt.initConfig({

        watch: {
            files: "content/less/style.less",
            tasks: 'less',
        },

        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "content/css/style.css": "content/less/style.less" // destination file and source file
                }
            }
        },

        browserSync: {
            files: {

                // Aplicando o recurso de Live Reload nos seguintes arquivos
                src: [
                    'content/css/*.css',
                    '**/*.html',
                    'js/**/*.js'
                ],

            },
            options: {

                // Definindo um IP manualmente
                host: "localhost",

                // Atribuíndo um diretório base
                server: {
                    baseDir: "."
                },

                // Integrando com a tarefa "watch"
                watchTask: true,

                // Sincronizando os eventos entre os dispositívos
                ghostMode: {
                    scroll: true,
                    links: true,
                    forms: true
                }
            },
        },
        ngtemplates: {
            app: {
                src: 'view/**/*.html',
                dest: 'js/cache/cache.js',
                options: {
                    url: function(url) {
                        return url.replace('view/', '');
                    },
                    bootstrap: function(module, script) {
                        return '/* jshint ignore:start */\nangular.module(\'cache.template\', []).run([\'$templateCache\', function ($templateCache) {\n' + script + '}])\n/* jshint ignore:end */';
                    }
                },
            }
        }
    });

    // Registrando tarefas customizadas
    grunt.registerTask('default', ["browserSync", "watch"]);
};
